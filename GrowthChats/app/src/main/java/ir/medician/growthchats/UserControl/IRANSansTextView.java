package ir.medician.growthchats.UserControl;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import ir.medician.growthchats.R;

/**
 * Created by salimi on 2/14/2016.
 */
public class IRANSansTextView extends TextView {

    public IRANSansTextView(Context context) {
        super(context);
        if (isInEditMode()) return;
        parseAttributes(null);
    }

    public IRANSansTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    public IRANSansTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    private void parseAttributes(AttributeSet attrs) {
        int typeface;
        if (attrs == null) { //Not created from xml
            typeface = IRANSans.IRANSans;
        } else {
            TypedArray values = getContext().obtainStyledAttributes(attrs, R.styleable.IRANSansTextView);
            typeface = values.getInt(R.styleable.IRANSansTextView_IRANSansTypeface, IRANSans.IRANSans);
            values.recycle();
        }
        setTypeface(getIRANSans(typeface));
    }

    public void setIRANSansTypeface(int typeface) {
        setTypeface(getIRANSans(typeface));
    }

    private Typeface getIRANSans(int typeface) {
        return getIRANSans(getContext(), typeface);
    }

    public static Typeface getIRANSans(Context context, int typeface) {
        switch (typeface) {
            case IRANSans.IRANSans:
                if (IRANSans.sIRANSans == null) {
                    IRANSans.sIRANSans = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans.ttf");
                }
                return IRANSans.sIRANSans;

            case IRANSans.IRANSans_Bold:
                if (IRANSans.sIRANSansBold == null) {
                    IRANSans.sIRANSansBold = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans-Bold.ttf");
                }
                return IRANSans.sIRANSansBold;

            case IRANSans.IRANSans_Light:
                if (IRANSans.sIRANSansLight == null) {
                    IRANSans.sIRANSansLight = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans-Light.ttf");
                }
                return IRANSans.sIRANSansLight;

            case IRANSans.IRANSans_Medium:
                if (IRANSans.sIRANSansMedium == null) {
                    IRANSans.sIRANSansMedium = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans-Medium.ttf");
                }
                return IRANSans.sIRANSansMedium;

            case IRANSans.IRANSans_ultralight:
                if (IRANSans.sIRANSansultralight == null) {
                    IRANSans.sIRANSansultralight = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans-ultralight.ttf");
                }
                return IRANSans.sIRANSansultralight;

            default:
                return IRANSans.sIRANSans;
        }
    }

    public static class IRANSans {

        public static final int IRANSans = 16;
        public static final int IRANSans_Bold = 17;
        public static final int IRANSans_Light = 18;
        public static final int IRANSans_Medium = 19;
        public static final int IRANSans_ultralight = 20;


        private static Typeface sIRANSans;
        private static Typeface sIRANSansBold;
        private static Typeface sIRANSansLight;
        private static Typeface sIRANSansMedium;
        private static Typeface sIRANSansultralight;
    }
}
