package ir.medician.growthchats;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;


public class HeadCircumFragment extends Fragment implements CalendarDatePickerDialogFragment.OnDateSetListener{

    ViewGroup rootView;
    private Toolbar mToolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView image;
    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private TextView DateBirthday, seekBarTextView, mResultTextView;
    private SeekBar seekBar;

    public HeadCircumFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_head_circum, container, false);
        ((MainActivity)getActivity()).getSupportActionBar().hide();
        image = (ImageView) rootView.findViewById(R.id.collapsImageView);
        image.setImageResource(R.drawable.photo1);

        collapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(getString(R.string.app_name));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        DateBirthday = (TextView) rootView.findViewById(R.id.dateBirthdayTextView);
        mResultTextView = (TextView) rootView.findViewById(R.id.calendarTextView);
        TextView button = (TextView) rootView.findViewById(R.id.calendarIconTextView);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(HeadCircumFragment.this);
                cdp.show(getFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });

        mResultTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayView(DateBirthday);
                mResultTextView.setHint("");
            }
        });

        seekBar = (SeekBar) rootView.findViewById(R.id.seekBar1);
        seekBarTextView = (TextView) rootView.findViewById(R.id.textView1);
        seekBarTextView.setText(seekBar.getProgress() +" "+ "Kg");
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                seekBarTextView.setText(progresValue + " " + "Kg");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Do something here, if you want to do anything at the start of
                // touching the seekbar
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Display the value in textview
                seekBarTextView.setText(progress + " " + "Kg");
            }
        });


        cutomActionBarTitle();
        DetailPageView();
        return rootView;
    }

    private void DetailPageView(){
        Button calculusButton = (Button)rootView.findViewById(R.id.calculusButton);
        calculusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) rootView.getContext()).ViewDetailFragment();
            }
        });
    }

    public void displayView(View activateView){
           //fade in from top
            activateView.setVisibility(View.VISIBLE);
            final Animation SlideDownAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
            activateView.startAnimation(SlideDownAnimation);
    }

    private void cutomActionBarTitle(){
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        mToolbar.hideOverflowMenu();

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        mResultTextView.setText(getString(R.string.date_picker_result_value, year, monthOfYear, dayOfMonth));
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        // Example of reattaching to the fragment
        super.onResume();
        CalendarDatePickerDialogFragment calendarDatePickerDialogFragment = (CalendarDatePickerDialogFragment) getFragmentManager()
                .findFragmentByTag(FRAG_TAG_DATE_PICKER);
        if (calendarDatePickerDialogFragment != null) {
            calendarDatePickerDialogFragment.setOnDateSetListener(this);
        }
    }
}
