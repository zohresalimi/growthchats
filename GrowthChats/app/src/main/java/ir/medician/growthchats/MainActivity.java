package ir.medician.growthchats;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener{

    private Toolbar mToolbar;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cutomActionBarTitle();
        ViewHomeFragment();

    }


    private void cutomActionBarTitle(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    public void ViewHomeFragment(){

        HomeFragment homeFragment = (HomeFragment)
                getSupportFragmentManager().findFragmentById(R.id.homeContainer);
        if (homeFragment != null) {

        } else {
            HomeFragment newFragment = new HomeFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container_body, newFragment);
            transaction.addToBackStack("tag");
            transaction.commit();
        }
    }

    public void ViewCircumFragment(){

        HeadCircumFragment circumFragment = (HeadCircumFragment)
                getSupportFragmentManager().findFragmentById(R.id.homeContainer);
        if (circumFragment != null) {

        } else {
            HeadCircumFragment newFragment = new HeadCircumFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container_body, newFragment);
            transaction.addToBackStack("tag");
            setTitle(getString(R.string.app_name));
            transaction.commit();
        }
    }

    public void ViewDetailFragment(){

        DetailFragment detailFragment = (DetailFragment)
                getSupportFragmentManager().findFragmentById(R.id.homeContainer);
        if (detailFragment != null) {

        } else {
            DetailFragment newFragment = new DetailFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container_body, newFragment);
            transaction.addToBackStack("tag");
            setTitle(getString(R.string.app_name));
            transaction.commit();
        }
    }


    @Override
    public void onFragmentInteraction(Uri uri) {
        ViewCircumFragment();

    }
}
